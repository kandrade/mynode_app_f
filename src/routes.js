const express = require('express');
const routes = express.Router();

const ProductController = require('./controllers/ProductController')

routes.get('/products', ProductController.index);
routes.get('/products/:id', ProductController.list);
routes.put('/products/:id', ProductController.update);
routes.delete('/products/:id', ProductController.remove);
routes.post('/products', ProductController.store)

module.exports = routes;