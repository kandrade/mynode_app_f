const mongoose = require('mongoose');

const Product = mongoose.model('Product');

module.exports = {
  async index(req, res){
    const { page = 1 } = req.query;
    const products = await Product.paginate({}, {page:page , limit:10});

    return res.json(products);
  },

  async list(req, res){
    const {id} = req.params;
    const product = await Product.findById(id);
    return res.status(200).json(product);
  },
  async update(req, res){
    const {id} = req.params;
    const product = await Product.findByIdAndUpdate(id, req.body, {new:true});
    return res.status(200).json(product);
  },
  async remove(req, res){
    const {id} = req.params;
    await Product.findByIdAndRemove(id);
    return res.status(204).json();
  },
  async store(req, res){
    const product = await Product.create(req.body);
    return res.status(200).json(product);
  }
}