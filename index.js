const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const requireDir = require('require-dir');
const User = require('./User-credentials');

const app = express();
const USER_CREDENTIALS = new User();

app.use(express.json());
app.use(cors());

function logger(request, response, next){
  const {method, url} = request;
  const logLabel = `[${method.toUpperCase()} ${url}]`;
  console.time(logLabel)
  console.timeEnd(logLabel)
  return next()
}

app.use(logger);

const MONGODB_URI = `mongodb+srv://${USER_CREDENTIALS.username}:${USER_CREDENTIALS.password}@cluster0-4hsoo.mongodb.net/test?retryWrites=true&w=majority`;

const MONGO_CONFIG = {
  useNewUrlParser: true,
  useUnifiedTopology: true
}



app.listen(3333, ()=> {
  mongoose.connect(MONGODB_URI, MONGO_CONFIG, 
    (err, res) => {
      if(err) {
        throw new Error("connection to mongo failed", err)
      } 
      console.log("connected with mongodb database")
    })
    console.log('server started at port http://localhost:3333/')
  })
  
  requireDir('./src/models')
  app.use("/api", require("./src/routes"));